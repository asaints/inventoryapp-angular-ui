export interface Product{
	productId: number;
	productName: string;
	productStock: number;
	productCategory: string;
}