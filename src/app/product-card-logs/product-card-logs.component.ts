import { Component, OnInit } from '@angular/core';

export interface Transaction{
	transactionId: number;
	transactionAdd: boolean;
	transactionQty: number;
	productId: number;
	transactionDate: string;
}

const TRANSACTIONS: Transaction[] = [
	{transactionId: 5, transactionAdd: true, transactionQty: 1, productId: 1, transactionDate: "Jun 5, 2019 11:25"},
	{transactionId: 4, transactionAdd: true, transactionQty: 1, productId: 1, transactionDate: "Jun 5, 2019 11:24"},
	{transactionId: 3, transactionAdd: false, transactionQty: 2, productId: 1, transactionDate: "Jun 5, 2019 11:23"},
	{transactionId: 2, transactionAdd: true, transactionQty: 1, productId: 1, transactionDate: "Jun 5, 2019 11:22"},
	{transactionId: 1, transactionAdd: true, transactionQty: 1, productId: 1, transactionDate: "Jun 5, 2019 11:21"},
];

@Component({
	selector: 'app-product-card-logs',
	templateUrl: './product-card-logs.component.html',
	styleUrls: ['./product-card-logs.component.css']
})
export class ProductCardLogsComponent implements OnInit {
	displayedColumnsTransactions: string[] = ['transactionDate', 'transactionQty'];
	dataSourceTransactions = TRANSACTIONS;
	
	constructor() { }

	ngOnInit() {
	}

}
