import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductCardLogsComponent } from './product-card-logs.component';

describe('ProductCardLogsComponent', () => {
  let component: ProductCardLogsComponent;
  let fixture: ComponentFixture<ProductCardLogsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductCardLogsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCardLogsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
