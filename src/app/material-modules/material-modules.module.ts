import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {MatButtonModule} from '@angular/material/button';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';
import {MatDividerModule} from '@angular/material/divider';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatTabsModule} from '@angular/material/tabs';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatChipsModule} from '@angular/material/chips';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatListModule} from '@angular/material/list';

@NgModule({
	declarations: [],
	imports: [
		CommonModule,
		MatButtonModule,
		MatCardModule,
		MatTableModule,
		MatDividerModule,
		MatGridListModule,
		MatToolbarModule,
		MatInputModule,
		MatIconModule,
		MatButtonToggleModule,
		MatTabsModule,
		MatDialogModule,
		MatTooltipModule,
		MatChipsModule,
		MatSnackBarModule,
		MatListModule
	],

	exports:[
		MatButtonModule,
		MatCardModule,
		MatTableModule,
		MatDividerModule,
		MatGridListModule,
		MatToolbarModule,
		MatInputModule,
		MatIconModule,
		MatButtonToggleModule,
		MatTabsModule,
		MatDialogModule,
		MatTooltipModule,
		MatChipsModule,
		MatSnackBarModule,
		MatListModule
	]
})

export class MaterialModulesModule { }
