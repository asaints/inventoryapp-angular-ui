import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { LeftCardContentComponent, AddProductDialog, CategoriesDialog } from './left-card-content/left-card-content.component';
import { RightCardComponent } from './right-card/right-card.component';
import { ProductCardHeaderComponent } from './product-card-header/product-card-header.component';
import { ProductCardLogsComponent } from './product-card-logs/product-card-logs.component';
import { ProductsSearchComponent } from './products-search/products-search.component';
import { ProductsTableComponent, SnackbarNotificationComponent } from './products-table/products-table.component';
import { HeaderToolbarComponent } from './header-toolbar/header-toolbar.component';

import {BrowserAnimationsModule, NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModulesModule} from "./material-modules/material-modules.module";

@NgModule({
	declarations: [
		AppComponent,
		LeftCardContentComponent,
		RightCardComponent,
		ProductCardHeaderComponent,
		ProductCardLogsComponent,
		ProductsSearchComponent,
		ProductsTableComponent,
		HeaderToolbarComponent,
		AddProductDialog,
		CategoriesDialog,
		SnackbarNotificationComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		BrowserAnimationsModule,
		HttpClientModule,
		FormsModule,
		MaterialModulesModule
	],
	entryComponents: [AddProductDialog, CategoriesDialog, SnackbarNotificationComponent],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule { }
