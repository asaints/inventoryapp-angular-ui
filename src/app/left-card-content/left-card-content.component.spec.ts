import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeftCardContentComponent } from './left-card-content.component';

describe('LeftCardContentComponent', () => {
  let component: LeftCardContentComponent;
  let fixture: ComponentFixture<LeftCardContentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeftCardContentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeftCardContentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
