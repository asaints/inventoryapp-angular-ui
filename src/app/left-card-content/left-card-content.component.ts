import { Component, OnInit, Inject} from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material/icon';
import {ProductService} from "../services/product-service/product.service";
import {MatSnackBar} from '@angular/material/snack-bar';

export interface DialogData {
	animal: string;
	name: string;
}

@Component({
	selector: 'app-left-card-content',
	templateUrl: './left-card-content.component.html',
	styleUrls: ['./left-card-content.component.css']
})

export class LeftCardContentComponent implements OnInit {
	animal: string;
	name: string;

	addProductModel: object = {};
	durationInSeconds = 5;

	openAddProductDialog(): void {
		const dialogRef = this.dialog.open(AddProductDialog, {
			width: '300px',
			data: {addProductModel: this.addProductModel}
		});

		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed');
			console.log(result)

			if(result){
				this.addProductModel = result;
				this.addProduct();
			}
			
		});
	}

	openCategoriesDialog(): void {
		const dialogRef = this.dialog.open(CategoriesDialog, {
			width: '500px',
			height: "500px",
			data: {name: this.name, animal: this.animal},
			disableClose: true
		});

		dialogRef.afterClosed().subscribe(result => {
			console.log('The dialog was closed');
			this.animal = result;
		});
	}

	addProduct(): void{
		let obs = this.ProductService.addProduct(this.addProductModel);

		obs.subscribe(data => {
			this.openSnackBar("Successfully added new product.");

			// this.getProducts();
		});
	}

	openSnackBar(message: string, ){
		this._snackBar.open(message, null, {
			duration: this.durationInSeconds * 1000,
		});
	}

	constructor(
		public dialog: MatDialog, 
		iconRegistry: MatIconRegistry, 
		sanitizer: DomSanitizer,
		private ProductService: ProductService,
		private _snackBar: MatSnackBar
	){
		iconRegistry.addSvgIcon('product', sanitizer.bypassSecurityTrustResourceUrl('assets/fridge.svg'));
		iconRegistry.addSvgIcon('category', sanitizer.bypassSecurityTrustResourceUrl('assets/list.svg'));
		iconRegistry.addSvgIcon('edit', sanitizer.bypassSecurityTrustResourceUrl('assets/edit.svg'));
		iconRegistry.addSvgIcon('delete', sanitizer.bypassSecurityTrustResourceUrl('assets/rubbish-bin.svg'));
		iconRegistry.addSvgIcon('check', sanitizer.bypassSecurityTrustResourceUrl('assets/checked.svg'));
		iconRegistry.addSvgIcon('close', sanitizer.bypassSecurityTrustResourceUrl('assets/close-button.svg'));
	}

	ngOnInit(){
		this.addProductModel['productName'] = "";
	}

}

@Component({
	selector: 'add-product-dialog',
	templateUrl: 'add-product-dialog.html',
})

export class AddProductDialog {
	constructor(
		public dialogRef: MatDialogRef<AddProductDialog>,
		@Inject(MAT_DIALOG_DATA) public data: DialogData
	) {}

	onNoClick(): void {
		this.dialogRef.close();
	}
}

@Component({
	selector: 'categories-dialog',
	templateUrl: 'categories-dialog.html',
})

export class CategoriesDialog {
	constructor(
		public dialogRef: MatDialogRef<CategoriesDialog>,
		@Inject(MAT_DIALOG_DATA) public data: DialogData
	) {}

	onNoClick(): void {
		this.dialogRef.close();
	}
}