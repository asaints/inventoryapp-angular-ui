import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {Product} from "../classes/product";
import {ProductService} from "../services/product-service/product.service";
import {DataSource} from "@angular/cdk/collections";
import {MatTableDataSource} from "@angular/material";
import {MatSnackBar} from '@angular/material/snack-bar';

const PRODUCTS: Product[] = [
	{productId: 1, productName: 'Corned Beef', productStock: 1, productCategory: 'Food'},
	{productId: 2, productName: 'Pride Detergent', productStock: 12, productCategory: 'Household'},
	{productId: 3, productName: 'Cotton', productStock: 1, productCategory: 'Hygiene'},
	{productId: 4, productName: 'Cooking Oil', productStock: 1, productCategory: 'Food - Cooking'},
];

@Component({
	selector: 'app-products-table',
	templateUrl: './products-table.component.html',
	styleUrls: ['./products-table.component.css']
})

export class ProductsTableComponent implements OnInit {
	displayedColumnsInventory: string[] = ['productName', 'productStock', 'productCategory'];
	dataSourceInventory: any;
	searchkey: string;
	inventoryMasterList: any;
	durationInSeconds = 5;

	constructor(
		private ProductService: ProductService,
		private _snackBar: MatSnackBar
	){}

	searchData(): void{
		let tempresults = [];

		var regex = RegExp(this.searchkey, 'g');

		this.inventoryMasterList.forEach(
			function(product){
				if(regex.test(product.productName)){
					tempresults.push(product);
				}
			}
		);

		this.dataSourceInventory = tempresults;
	}

	getProducts(): void{
		let obs = this.ProductService.getProducts();

		obs.subscribe(data => {
			console.log(data);

			for(var key in data){
				data[key]['edit'] = false;
				// console.log(data[key]['productCategory'] !== "");
				data[key]['productCategory'] = (data[key]['productCategory'] !== null? data[key]['productCategory'].split(','):[]);
				data[key]['backupProductName'] = data[key]['productName'];
				console.log(data[key]['productCategory']);
			}

			this.dataSourceInventory = data;
			this.inventoryMasterList = data;
		})
	}

	saveEditProductChanges(product: Product): void{
		delete product['edit'];
		delete product['productCategory']; //temporary

		let obs = this.ProductService.editProduct(product);

		obs.subscribe(data => {
			this.openSnackBar("Successfully update product information.");

			this.getProducts();
		});
	}

	deleteProduct(product: Product): void{
		let obs = this.ProductService.deleteProduct(product.productId);

		obs.subscribe(data => {
			this.openSnackBar("Successfully deleted selected product.");

			this.getProducts();
		});
	}

	openSnackBar(message: string, ){
		this._snackBar.open(message, null, {
			duration: this.durationInSeconds * 1000,
		});
	}

	ngOnInit(){
		this.getProducts();
	}
}

@Component({
	selector: 'snackbar-notification',
	templateUrl: 'snackbar-notification.html',
	styles: ['./products-table.component.css'],
})

export class SnackbarNotificationComponent{}