import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../../classes/product";
import {Observable, of} from "rxjs";

@Injectable({
	providedIn: 'root'
})

export class ProductService {
	hostname = "localhost";
	port = "5000";
	protocol = "http://";


	getProducts(){
		let method = "getProducts";
		let url = `${this.protocol}${this.hostname}:${this.port}/${method}`;

		return this.http.get(url);
	}

	addProduct(productmodel){
		let path = `getProducts/add`;
		let url = `${this.protocol}${this.hostname}:${this.port}/${path}`;

		return this.http.post<Product>(url, productmodel);
	}
	
	deleteProduct(id){
		let path = `getProducts/${id}/delete`;
		let url = `${this.protocol}${this.hostname}:${this.port}/${path}`;

		return this.http.delete(url);
	}

	editProduct(product){
		let path = `getProducts/${product.productId}/edit`;
		let url = `${this.protocol}${this.hostname}:${this.port}/${path}`;

		return this.http.put<Product>(url, product);
	}

	constructor(private http: HttpClient){}
}
